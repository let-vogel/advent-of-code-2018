f = open("input", "r").read().split("\n")

areas = []

for v in f:
  arr = v.split(" ")
  start = list(map(int, arr[2].replace(":", "").split(",")))
  size = list(map(int, arr[3].split("x")))

  area = []

  for x in range(size[0]):
    for y in range(size [1]):
      area.append(int(f"{start[0] + x}{start[1] + y}"))
  
  areas.append(area)

overlapping_areas = []
for a in areas:
  for b in areas:
    if a != b and len(list(set(a).intersection(b))):
      overlapping_areas.append(a)

fareas = [val for sublist in areas for val in sublist]
foverlapping_areas = [val for sublist in overlapping_areas for val in sublist]
result = list(set(fareas) - set(foverlapping_areas))

print(min(result))
      

