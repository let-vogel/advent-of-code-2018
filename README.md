# Advent of Code 2018!

| day | part 1 | part 2 | language |
|:---:|:------:|:------:|:--------:|
|  [1](https://adventofcode.com/2018/day/1)  |    ★   |        |  Elixir  |
|  [2](https://adventofcode.com/2018/day/2)  |    ★   |    ★   |  Elixir  |
|  [3](https://adventofcode.com/2018/day/3)  |    ★   |    ★   |  Elixir / Python|