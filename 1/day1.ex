defmodule Advent do
  def part1 do
    {_, file} = File.read("input")
    file = String.split(file, "\n")

    Enum.sum(for n <- file, do: String.to_integer(n))
  end

  def part2 do
    {_, arr} = File.read("input")
    arr = String.split(arr, "\n")
    arr = for n <- arr, do: String.to_integer(n)

    arr
    |> Enum.with_index
    |> Enum.reduce(0, fn({number, index}, acc) -> [number | acc + number] end) 
  end
end
