defmodule Advent do
  def part1 do
    {_, file} = File.read("input")
    file = String.split(file, "\n")

    two_times = for n <- file do
      count_duplicates(n)
      |> String.contains?("2")
    end
    two_times = Enum.count(two_times, &(&1 == true))

    three_times = for n <- file do
      count_duplicates(n)
      |> String.contains?("3")
    end
    three_times = Enum.count(three_times, &(&1 == true))

    two_times * three_times
  end

  def part2 do
    {_, file} = File.read("input")
    file = String.split(file, "\n")

    for j <- file, k <- file do
      if String.jaro_distance(j, k) == 0.9743589743589745 do
        IO.puts j
        IO.puts k
      end
    end
  end

  defp count_duplicates(str) do
    str
    |> String.graphemes
    |> Enum.sort
    |> Enum.chunk_by(fn arg -> arg end)
    |> Enum.map(fn arg -> to_string(arg) end)
    |> Enum.reduce("", fn(arg, acc) -> acc <> to_string(String.length(arg)) <> String.first(arg) end)
  end
end